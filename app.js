const choo = require('choo')
const devtools = require('choo-devtools')

const app = choo()

const mainView = require('./views/main.js')

// Stores
const producto = require('./stores/producto.js')
const api = require('./stores/api.js')
const anim = require('./stores/animacion.js')
const auth = require('./stores/auth.js')
const keyboardShortucts = require('./stores/keyboardShortcuts.js')
const zoom = require('./components/Image.js').store

app.use((state, emitter) => {
  state.title = 'PIDDEF'
  emitter.emit('DOMTitleChange', state.title)
})

document.addEventListener('contextmenu', (e) => {
  e.preventDefault()
})

if (process.env.NODE_ENV === 'development') {
  app.use(devtools())
}

app.use(auth)
app.use(producto)
app.use(api)
app.use(anim)
app.use(keyboardShortucts)
app.use(zoom)

app.route('/', mainView)

app.mount('body')
