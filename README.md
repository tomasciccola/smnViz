# WEB SMN

Repositorio para alojar la web de visualización de productos del S.M.N.

Especificaciones en Documento a parte

## STACK

* [NPM](https://npmjs.com) (gestor de paquetes)
* [Browserify](https://github.com/browserify/browserify) (server de desarrollo)
* [Choo](https://github.com/choojs/choo) (framework de frontend)
* [Sheetify](https://github.com/stackcss/sheetify) (framework de css)

## Para correr server de desarrollo

```
  npm install && npm run watch

```

## Para exportar como web estática

```
  npm install && npm run build

```
## TODO

* ~~ Escribir API para ya agarrar imgs. Hay imágenes con intervalos distintos y cuyos segundos no están redondeados (La solución acá sería ver coordinación con server. O que hagan imgs redondeadas - como los mosaicos, por ejemplo - o que tengan una api que me de una lista de nombres de las últimas imágenes).~~ Esto se resuelve con el webService

* Diseño
* Empezar a portar cuestiones de HaNis:
  * Controles portados: play/pause, loop/rock, loopAt, next, prev. frame + - y frame primero / ultimo. Reset zoom. click derecho sobre imagen zoom/ izquierdo zoom out y alt+click zoom reset.
  * Controles de actualización de imagen: Cada cuanto refresh (auto, 5, 10min). Botón de refrescar y reiniciar conteo.
  * ~~ Zoom in y Zoom out (con rueda de mouse). Avanzar y retroceder frames (flechitas).~~
  * ~~ Keybindings: PlayPause(barra espaciadora)~~
* Botónes de Actualización de imágen. Uno para desactivar y otro para refresh (que puede ser el mismo indicador de producto desactualizado)
* ~~ Token para web service ~~
* ~~ Loop satélites : 6 12 24~~
* ~~ Lista de productos colapsables~~
* ~~ Ver tema de resolucion en el zoom (cuando achico la imagen, HaNis parece que baja la resolución) ~~
* ~~ Hotkeys (ver cuales son y cual falta) ~~
* ~~ Velocidad de animación seleccionable con + - ~~
* Mejorar la carga de imágenes (~~cargar en paralelo~~, guardar productos pasados)
* Agregar licencia

### INTEGRACION WEB TOKEN
~~ 1. Re esquematizar la pagina teniendo las regiones como nav. Por ahora las regiones las defino yo y asignaré manualmente la region a cada producto. Tengo que tener un map de producto a region~~
~~ 2. En la barra de la izquierda aparecerán los tipos de productos (Radar, Satélite) ~~
~~ 3. Cuando hago click en  un tipo de producto, necesito pedir la lista con los links y guardar todo en un objeto ~~
~~ 4. En ese objeto iré guardando la data de imagenes que pida y simplemente hago una referencia de eso al producto actual a mostrar ~~
5. Cada producto además activará un timer que avise cuando el producto está desactualizado

## LICENSE

GPLv2
