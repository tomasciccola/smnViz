const http = require('http')
const sendJson = require('send-data/json')

const server = http.createServer((req, res) => {
  if (req.url === '/auth') {
    console.log('AUTH')
    if (isTokenExpired(authStorage.authDate)) {
      console.log('necesito actualizar token')
      auth((err, body) => {
        if (err) console.error(err)
        saveCredentials(body)
        sendJson(req, res, {
          statusCode: 200,
          body: body })
      })
    } else {
      console.log('no hace falta actualizar token')
      sendJson(req, res, {
        statusCode: 200,
        body: authStorage.token
      })
    }
  } else {
    res.end('Ruta desconocida')
  }
})

server.listen(8000)

// AUTH
const fs = require('fs')
const xhr = require('xhr-request')
const moment = require('moment')
const AUTH_URL = 'https://api-test.smn.gob.ar/v1/api-token/auth'
const CREDENTIALS = { username: 'tciccola', password: 'Sv1ePf54' }

const authStorage = JSON.parse(fs.readFileSync('auth.json').toString())
var DATE_RFC2822 = 'ddd, DD MMM YYYY HH:mm:ss ZZ'

function auth (cb) {
  const obj = {
    method: 'POST',
    body: JSON.stringify(CREDENTIALS),
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    }
  }
  xhr(AUTH_URL, obj, (err, body) => {
    if (err) return cb(err, null)
    return cb(null, JSON.parse(body))
  })
}

function saveCredentials (body) {
  authStorage.token = body['token']
  authStorage.authDate = moment().format(DATE_RFC2822)
  fs.writeFileSync('auth.json', JSON.stringify(authStorage))
  console.log('credencial actualizada')
}

function isTokenExpired (fecha) {
  if (!fecha) return true
  let ayer = moment().subtract(1, 'd')
  return moment(fecha).isBefore(ayer)
}
