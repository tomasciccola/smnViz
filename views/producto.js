const html = require('choo/html')
const css = require('sheetify')

const ProductoCtrl = require('../components/ProductoCtrl.js')
var ctrls = new ProductoCtrl()

const Image = require('../components/Image.js').View
var img = new Image()

module.exports = (state, emit) => {
  const style = css`
    :host{
      grid-area:producto;
    }

  `
  // Armo un objeto donde sólo le paso el estado que necesita saber para funcionar
  const ctrlsObj = {
    productsExist: state.productoActual.id ? 1 : 0,
    loop: state.loop,
    playState: state.playState,
    atProductType: state.atProductType,
    loopOn: state.loopOn,
    interval: state.updateProductInterval
  }
  const imgObj = {
    producto: state.productoActual.imgs || [],
    atImg: state.atProductImg,
    loading: state.loadingProducto
  }

  return html`
    <div class=${style}>
      ${ctrls.render(ctrlsObj, emit)}
      ${img.render({ imgState: imgObj, zoom: state.zoom }, emit)}
    </div>
  `
}
