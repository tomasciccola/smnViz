const html = require('choo/html')
const css = require('sheetify')

// Vistas
const Logo = require('../components/Logo.js')
const logo = new Logo()
const Nav = require('../components/Nav.js')
const nav = new Nav()
const ProductoMenu = require('../components/ProductoMenu.js')
const prodMenu = new ProductoMenu()
const producto = require('./producto.js')
const tools = require('./tools.js')
const footer = require('./footer.js')

module.exports = (state, emit) => {
  const style = css`
  :host {
    height:100vh;
    overflow:hidden;
    display:grid;
    grid-template-columns: minmax(0, 0.2fr) minmax(0,0.6fr) minmax(0,0.2fr);
    grid-template-rows: minmax(0,0.12fr) minmax(0,0.80fr) minmax(0,0.125fr);
    grid-template-areas:
    "logo nav . "
    "productoMenu producto tools"
    "footer footer footer";
    background: var(--mainBg);
    color: var(--mainFg);
    font-family:'Inconsolata';
  }

  :host {
    --mainBg:#efefef;
    --mainFg:#1c1c1c;
    --blue:#1191d0b8;
  }
  `
  var prodMenuObj = {
    'region': state.atRegion,
    'productos': state.productos,
    'productoActual': state.productoActual
  }
  return html`
  <body class=${style}>
    ${logo.render()}
    ${nav.render(emit)}
    ${prodMenu.render(prodMenuObj, emit)}
    ${producto(state, emit)}
    ${tools(state, emit)}
    ${footer(state, emit)}
  </body>
  `
}
