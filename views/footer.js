const html = require('choo/html')
const css = require('sheetify')

module.exports = (state, emit) => {
  const style = css`
  :host {
    grid-area: footer;
    padding: 25px 50px 0 50px;
    margin-top: 25px;
    text-align:justify;
    color:grey;
    border-top: 2px solid grey;
  }
  `

  return html`
    <footer class=${style}>
    Descargo de Responsabilidad: se aconseja a los pilotos y demás usuarios de este sitio web obtener asesoramiento meteorológico en las Oficinas de Vigilancia Meteorológicas (OVM) y a las Oficinas de Información Meteorológica (OIM) antes de viajar. El usuario acepta toda la responsabilidad y riesgo asociados con la utilización de la información contenida en este sitio. El usuario expresamente libera de responsabilidad al Servicio Meteorológico Nacional de Argentina, sus empleados y agentes.
    </footer>
  `
}
