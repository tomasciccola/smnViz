const html = require('choo/html')
const css = require('sheetify')

module.exports = (state, emit) => {
  const style = css`
    :host{
      grid-area:tools;
    }
  `
  return html`
    <div class=${style}>
    </div>
  `
}
