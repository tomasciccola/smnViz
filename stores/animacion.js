module.exports = (state, emitter) => {
  // Player variables
  state.playState = false
  state.atProductImg = 0
  state.animSpeed = 200
  state.animSpeedIncrement = 50
  state.animLoop = null
  state.loopOn = 12
  state.loop = true
  state.forward = true

  emitter.on('next', () => {
    state.atProductImg = Math.abs(loopOver(1))
    emitter.emit('render')
  })

  emitter.on('first', () => {
    state.atProductImg = 0
    emitter.emit('render')
  })

  emitter.on('prev', () => {
    state.atProductImg = Math.abs(loopOver(-1))
    emitter.emit('render')
  })

  emitter.on('last', () => {
    state.atProductImg = state.productoActual.imgs.length - 1
    emitter.emit('render')
  })

  function loopOver (direction) {
    return state.productoActual.imgs.length !== 0
      ? (state.atProductImg + (1 * direction)) % (state.loopOn)
      : 0
  }

  emitter.on('playPause', () => {
    state.playState = !state.playState
    if (state.playState) {
      emitter.emit('startAnim')
    } else {
      emitter.emit('stopAnim')
    }
    emitter.emit('render')
  })

  emitter.on('loopOn', (num) => {
    state.loopOn = num
    emitter.emit('render')
  })

  emitter.on('rockOrLoop', () => {
    state.loop = !state.loop
    if (state.loop) state.forward = true
    emitter.emit('render')
  })

  emitter.on('startAnim', () => {
    state.animLoop = setInterval(() => {
      state.forward ? emitter.emit('next') : emitter.emit('prev')
      checkRock()
    }, state.animSpeed)
  })

  emitter.on('stopAnim', () => {
    clearInterval(state.animLoop)
  })

  emitter.on('speedUp', () => {
    if (state.animSpeed > 50) state.animSpeed -= state.animSpeedIncrement
    if (state.playState) {
      emitter.emit('stopAnim')
      emitter.emit('startAnim')
      emitter.emit('render')
    }
  })

  emitter.on('speedDown', () => {
    if (state.animSpeed <= 2000) state.animSpeed += state.animSpeedIncrement
    if (state.playState) {
      emitter.emit('stopAnim')
      emitter.emit('startAnim')
      emitter.emit('render')
    }
  })

  /* ------ keyboardShortucts ------ */

  emitter.on('key:right', () => {
    emitter.emit('next')
  })

  emitter.on('key:left', () => {
    emitter.emit('prev')
  })

  emitter.on('key:space', () => {
    emitter.emit('playPause')
  })

  emitter.on('key:plus', () => {
    emitter.emit('speedUp')
  })

  emitter.on('key:minus', () => {
    emitter.emit('speedDown')
  })

  /* --------------------------------- */

  function checkRock () {
    if ((!state.loop) &&
      ((state.atProductImg === state.loopOn - 1) || (state.atProductImg === 0))) {
      state.forward = !state.forward
    }
  }
}
