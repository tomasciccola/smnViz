const xhr = require('xhr')
const getProductById = require('./getProductById.js')

module.exports = (state, emitter) => {
  emitter.on('getProduct', (product) => {
    var tasks = buildTasks(state.productoActual.list, state.atProductType)
    var runner = buildRunner(tasks, (err, data, idx) => {
      if (err)console.error(err)

      var p = getProductById(state.productos, product)
      p.imgs[idx] = URL.createObjectURL(data)
      emitter.emit('render')
    })

    runner()
  })

  emitter.on('updateProduct', (newPaths) => {
    var tasks = buildTasks(newPaths, state.atProductType)
    var runner = buildRunner(tasks, (err, data, idx) => {
      if (err)console.error(err)

      var imgs = state.productoActual.imgs
      var invIdx = invertRange(idx, 0, imgs.length - 1)
      imgs[invIdx] = URL.createObjectURL(data)
      emitter.emit('render')
    })

    runner()
  })
}

function buildTasks (paths, type) {
  const URL_ROOTS = {
    'Radares': 'estaticos.smn.gob.ar/vmsr/radar/',
    'Satélites': 'estaticos.smn.gob.ar/vmsr/satelite/'
  }
  // Opts for request
  var PROXY
  if (process.env.NODE_ENV === 'development') {
    PROXY = `http://localhost:1337`
  } else {
    PROXY = 'hola'
  }
  const opts = {
    responseType: 'blob'
  }
  return paths.slice().reverse()
    .map(url => `${URL_ROOTS[type]}${url}`).map(buildTask)

  function buildTask (url) {
    return function (cb) {
      xhr(`${PROXY}/${url}`, opts, cb)
    }
  }
}

function buildRunner (tasks, cb) {
  var running = 0
  var completed = 0
  var index = 0
  var concurrency = 3

  function runner () {
    while (running < concurrency && index < tasks.length) {
      var task = tasks[index]
      task(onDoneTask(index))
      index++
      running++
    }
  }

  function onDoneTask (idx) {
    return function (err, data) {
      if (err) return cb(err, null)

      completed++
      running--
      runner()

      cb(null, data.body, idx)

      if (completed === tasks.length) {
        console.log('terminé')
      }
    }
  }

  return runner
}

function invertRange (val, i, o) {
  return map(val, i, o, o, i)
}

function map (val, inMin, inMax, outMin, outMax) {
  return (val - inMin) * (outMax - outMin) / (inMax - inMin) + outMin
}
