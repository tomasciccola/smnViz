module.exports = function (state, emitter) {
  document.onkeyup = function (ev) {
    switch (ev.key) {
      case 'ArrowRight':
        emitter.emit('key:right')
        break
      case 'ArrowLeft':
        emitter.emit('key:left')
        break
      case ' ':
        emitter.emit('key:space')
        break
      case '+':
        emitter.emit('key:plus')
        break
      case '-':
        emitter.emit('key:minus')
        break
      case 'Alt':
        emitter.emit('keyup:alt')
    }
  }
  document.onkeydown = function (ev) {
    switch (ev.key) {
      case 'Alt':
        emitter.emit('keydown:alt')
    }
  }
}
