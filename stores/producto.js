const getProductList = require('./getProducts.js')
const getProductById = require('./getProductById.js')

module.exports = (state, emitter) => {
  state.atProductType = ''
  state.atRegion = ''
  state.loadingProducto = false

  state.productos = {}
  state.productoActual = {}
  state.productTypes = ['Radares', 'Satélites']

  state.checkIfOutdatedProductInterval = 2000
  state.updateProductInterval = 0
  state.outdatedProduct = false

  emitter.on('authorized', () => {
    state.productTypes.map(type => {
      getProductList(type, state.token, gotProductList(type))
    })
  })

  function gotProductList (t) {
    return function (err, data) {
      if (err)console.error(err)
      state.productos[t] = JSON.parse(data)
      emitter.emit('render')
    }
  }

  emitter.on('regionSelected', function (region) {
    state.atRegion = region
    emitter.emit('render')
  })

  emitter.on('productTypeSelected', function (productType) {
    // Los radares producen de a 12 mientras que los satélites de a 24
    state.atProductType = productType
    state.loopOn = state.atProductType === 'Radares' ? 12 : 24
  })

  emitter.on('newProduct', function (product) {
    // le doy pausa a la animación
    if (state.playState) emitter.emit('playPause')
    // Primero, matá el timer que se fija si está desactualizado
    clearInterval(state.productoActual.horaCheker)
    state.productoActual = getProductById(state.productos, product)
    // Si ya existe el producto sólo hace falta referenciarlo desde el producto actual
    if (state.productoActual.imgs) return emitter.emit('render')

    state.productoActual.imgs = []
    state.loadingProducto = true
    emitter.emit('getProduct', product)
    // Para setear el intervalo en el que checkeo. Si es cero (auto update)
    // entonces se usa el intervalo de checkeo
    // Sino se checkea cada 5 o 10 mins
    setUpdateTimer()
    emitter.emit('render')
  })

  emitter.on('updateOn', (val) => {
    state.updateProductInterval = val
    emitter.emit('render')
    clearInterval(state.productoActual.horaChecker)
    setUpdateTimer()
  })

  function setUpdateTimer () {
    var interval = state.updateProductInterval === 0
      ? state.checkIfOutdatedProductInterval
      : state.updateProductInterval * 60 * 1000
    state.productoActual.horaChecker = setInterval(checkIfProductoDesactualizado, interval)
  }

  function checkIfProductoDesactualizado () {
    getProductList(state.atProductType, state.token, function (err, data) {
      if (err) console.error(err)
      // Dame de vuelta la lista de urls
      var updatedProduct = JSON.parse(data)
        .reduce((prod, p) => p.id === state.productoActual.id ? p : prod, {})

      // Si hay data nueva (es distinta a la que tengo yo...)
      var needToUpdate = !arraysAreEqual(state.productoActual.list, updatedProduct.list)

      if (needToUpdate) {
        state.outdatedProduct = true
        if (state.updateProductInterval === 0) {
          updateProduct(updatedProduct)
        }
      }
    })
  }

  function updateProduct (newProd) {
    console.log('actualizando imagenes')
    // 1. Que elementos del nuevo array no están en el viejo?
    var diff = newProd.list.filter(x => !state.productoActual.list.includes(x))
    console.log(diff)
    // 2. Reemplazo el nuevo array de URLS por el viejo
    state.productoActual.list = newProd.list
    // 3. Tengo que shiftear el array de imagenes la izquierda tantas veces como diff.length
    var temp = Array(state.productoActual.imgs.length)
    for (let i = 0; i < temp.length - diff.length; i++) {
      temp[i] = state.productoActual.imgs[i + diff.length]
    }
    state.productoActual.imgs = temp
    emitter.emit('updateProduct', diff)
  }
}

function arraysAreEqual (arr1, arr2) {
  if (arr1.length !== arr2.length) return false

  for (let i = 0; i < arr1.length; i++) {
    if (arr1[i] !== arr2[i]) return false
  }
  return true
}
