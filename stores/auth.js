const xhr = require('xhr')

module.exports = (state, emitter) => {
  var AUTH_URL

  if (process.env.NODE_ENV === 'development') {
    AUTH_URL = 'http://localhost:8000/auth'
  } else {
    AUTH_URL = 'https://smn-auth.herokuapp.com'
  }

  emitter.on('DOMContentLoaded', () => {
    xhr.get(AUTH_URL, (err, res) => {
      if (err)console.error(err)
      if (res.statusCode === 200) {
        state.token = res.body.replace(/"/g, '')
        state.authorized = true
        emitter.emit('authorized')
      } else {
        console.error('ERROR', res.statusCode)
      }
    })
  })
}
