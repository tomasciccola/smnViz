
module.exports = function (products, p) {
  return products[p.type].reduce((prod, elem) => {
    if (elem.id === p.id) {
      return elem
    } else {
      return prod
    }
  }, {})
}
