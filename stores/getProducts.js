const xhr = require('xhr')

const ROOT_URL = 'https://api-test.smn.gob.ar/v1/images'

const prodTypeMap = {
  'Satélites': 'satellite',
  'Radares': 'radar'
}
module.exports = (type, token, cb) => {
  const obj = {
    method: 'get',
    uri: `${ROOT_URL}/${prodTypeMap[type]}`,
    headers: {
      'Authorization': `JWT ${token}`
    }
  }
  xhr(obj, (err, res) => {
    if (err) return (err, null)
    // No autorizado
    if (res.statusCode === 401) {
      return cb({ status: 401 }, null)
    }
    return cb(null, res.body)
  })
}
