const html = require('choo/html')
const css = require('sheetify')
const Nanocomponent = require('nanocomponent')

css('../assets/fontawesome/css/all.css')

module.exports = class ProductoCtrl extends Nanocomponent {
  constructor () {
    super()

    // Inicializo variables globales de la clase
    this.state = null
    this.emit = null

    this.style = css`
    :host {
      display:flex;
      flex-direction:row;
      justify-content: space-evenly;
      flex-wrap: wrap;
      padding:4px;
      max-height:5vh;
      margin-top:-30px;
      margin-bottom:30px;
    }
    button {
      border:none;
      background:var(--mainBg);
      color:var(--mainFg);
      font-size: 1.9em;
      align-content:center;
      padding:0px 6px 0px 6px;
      outline:none;
    }

    button:active{
      animation:glow 1.0s;
    }

    @keyframes glow{
      0%{
        color:var(--mainFg);
      }
      50%{
        color:var(--blue);
      }
      0%{
        color:var(--mainFg);
      }
    }

    .ctrlSection{
      display:flex;
      flex-direction:column;
    }

    .ctrlSection h3{
      text-align:center;
      font-size:0.75em;
    }

    .ctrls {
      display:flex;
      justify-content:space-evenly;
    }

    button.selected{
      color:var(--blue);
    }

    `
    // Bindeo metodos para poder usar this dentro de ellxs
    this.prev = this.prev.bind(this)
    this.next = this.next.bind(this)
    this.first = this.first.bind(this)
    this.last = this.last.bind(this)
    this.playPause = this.playPause.bind(this)
    this.loopOn = this.loopOn.bind(this)
    this.rockOrLoop = this.rockOrLoop.bind(this)
    this.speedUp = this.speedUp.bind(this)
    this.speedDown = this.speedDown.bind(this)
    this.updateOn = this.updateOn.bind(this)
  }
  // Métodos de la clase
  updateOn (val) {
    return (e) => this.emit('updateOn', val)
  }
  prev () {
    this.emit('prev')
  }

  next () {
    this.emit('next')
  }

  first () {
    this.emit('first')
  }

  last () {
    this.emit('last')
  }

  playPause () {
    this.emit('playPause')
  }

  loopOn (num) {
    return (e) => this.emit('loopOn', num)
  }

  rockOrLoop () {
    this.emit('rockOrLoop')
  }

  speedUp () {
    this.emit('speedUp')
  }

  speedDown () {
    this.emit('speedDown')
  }

  createElement (state, emit) {
    var that = this
    this.state = state// Object.assign({}, state)
    this.emit = emit
    // var loop = this.state.atProductType === 'Radares' ? 12 : 24
    return this.state.productsExist
      ? html`
    <div class=${this.style}>
      <div class="ctrlSection">
        <h3>Actualización</h3>
        <div class="ctrls">
          <button class="${checkState(that.state.interval, 5)}" 
          onclick=${this.updateOn(5)}> 5 </button>
          <button class=${checkState(that.state.interval, 10)} 
          onclick=${this.updateOn(10)}> 10 </button>
          <button class="${checkState(that.state.interval, 0)}" 
          onclick=${this.updateOn(0)}> auto </button>
        </div>
      </div>
      <div class="ctrlSection">
        <h3>Loop</h3>
      ${this.state.atProductType === 'Radares'
    ? createloopOnRadarBtns()
    : createloopOnSatelliteBtns()}
      </div>
      <div class="ctrlSection">
        <h3>Reproducción </h3>
        <div class="ctrls">
          <button onclick=${this.first} class="fas fa-step-backward"></button>
          <button onclick=${this.prev} class="fas fa-backward"></button>
          <button onclick=${this.playPause} class=${this.state.playState ? 'fas fa-pause' : 'fas fa-play'}></button>
          <button onclick=${this.next} class="fas fa-forward"></button>
          <button onclick=${this.last} class="fas fa-step-forward"></button>
        </div>
      </div>
      <div class="ctrlSection">
        <h3>Velocidad</h3>
        <div class="ctrls">
          <button onclick=${this.rockOrLoop} class=${this.state.loop ? 'fas fa-exchange-alt' : 'fas fa-redo'}></button>
          <button onclick=${this.speedUp}> + </button>
          <button onclick=${this.speedDown}> - </button>
        </div>
      </div>
    </div>`
      : html`<div></div>`

    function checkState (elem, cond) {
      return elem === cond
        ? 'selected'
        : ''
    }

    function createloopOnRadarBtns () {
      return html`
      <div class="ctrls">
        <button class=${checkState(that.state.loopOn, 3)} 
        onclick=${that.loopOn(3)}> 3 </button>
        <button class=${checkState(that.state.loopOn, 6)} 
        onclick=${that.loopOn(6)}> 6 </button>
        <button class=${checkState(that.state.loopOn, 12)} 
        onclick=${that.loopOn(12)}>12</button>
      </div>
      `
    }

    function createloopOnSatelliteBtns () {
      return html`
      <div class="ctrls">
        <button class=${checkState(that.state.loopOn, 6)} 
        onclick=${that.loopOn(6)}> 6 </button>
        <button class=${checkState(that.state.loopOn, 12)} 
        onclick=${that.loopOn(12)}> 12 </button>
        <button class=${checkState(that.state.loopOn, 24)} 
        onclick=${that.loopOn(24)}>24</button>
      </div>
      `
    }
  }

  update (state, emit) {
    return state.loop !== this.state.loop ||
      state.playState !== this.state.playState ||
      state.productsExist !== this.state.productsExist ||
      state.loopOn !== this.loopOn ||
      state.interval !== this.interval
  }
}
