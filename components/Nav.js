const html = require('choo/html')
const css = require('sheetify')
const Nanocomponent = require('nanocomponent')

const navLinks = require('../data/nav.json')

module.exports = class NavLinks extends Nanocomponent {
  constructor () {
    super()

    this.emit = null

    this.style = css`
    :host {
      grid-area:nav;
      display:flex;
      flex-direction:row;
      justify-content:space-evenly;
      background:var(--mainBg);
      margin-top:1em;
      margin-left:3.0em;

    }

    .navBtn{
        text-align:center;
        padding: 0px 6px 0 6px;
        margin: 0px 3px 0px 3px;
        height:45%;
        width:100%;
        font-size:1.2em;
        background:var(--blue);
        color:var(--mainFg);
        border:2px solid var(--mainFg);
        border-radius: 4px;
    }

    .navBtn:hover {
      background-color:var(--mainBg); color:var(--mainFg);
    }

  `
  }

  createElement (emit) {
    return html`
  <nav class=${this.style}>
    ${navLinks.map(buildNav)}
  </nav>
  `
    function buildNav (region) {
      return html`
      <button class="navBtn" onclick=${regionSelected}>${region}</button>
      `
      function regionSelected () {
        emit('regionSelected', region)
      }
    }
  }

  update () {
    return false
  }
}
