const html = require('choo/html')
const css = require('sheetify')
const Nanocomponent = require('nanocomponent')

const regionMap = require('../data/regionMap.json')
const colmaxMap = require('../data/colmaxMap.json')

module.exports = class ProductoMenu extends Nanocomponent {
  constructor () {
    super()
    this.region = null
    this.emit = null

    this.style = css`
    :host {
      grid-area:productoMenu;
      margin-top:2.5em;
      margin-left:1em;
      border-right:2px solid var(--mainFg);
    }

    .productTypeTitle{
      font-size:2.2em;
      font-weight:bold;
    }

    .subProductTypeTitle{
      font-size:1.5em;
    }


    .scrollable{
      overflow-y:auto;
      height:65vh;
      padding-right:8px;
    }

    ul{
      display: flex;
      flex-direction:column;
      justify-content:space-evenly;
      list-style-type:none;
      margin:0;
      padding:0;
    }

    li{
      font-size:1.2em;
      cursor:pointer;
      padding:8px;
      color:var(--mainFb);
      text-align:left;
      animation:slide-down 0.5s;
    }

    li:nth-child(odd){
      background:#c8c7c7;
    }

    li:nth-child(even){
      background:#c8c7c7;
    }

    li.selected {
      background:var(--blue);
    }


    li:hover {
      background-color:#ddd; color:#1c1c1c;
    }
    li.selected:hover{
      background:var(--blue);
    }

    @keyframes slide-down{
      0% {
        opacity:0;
        transform: translateY(-40px);
      }
      100%{
        opacity:1;
        transform: translateY();
      }
    }
    `
  }

  createElement (state, emit) {
    this.region = state.region
    this.productos = state.productos
    this.actual = state.productoActual
    this.emit = emit
    var that = this

    return html`
    <div class=${this.style}>
      <h3 class="productTypeTitle"> ${this.region || 'Productos'} </h3>
      <div class="scrollable">
      ${this.productos ? Object.keys(this.productos).map(buildProductTypeList) : ''} 
      </div>
    </div>
    `
    function buildProductTypeList (type) {
      var productsList = []
      if (type === 'Satélites') {
        productsList = that.productos[type].filter(pr => regionMap[that.region] === pr.region)
      } else { // RADARES
        var removeEmptyProduct = p => p.list.length !== 0

        productsList = that.productos[type]
          .filter(pr => that.region === pr.product || regionMap[that.region] === pr.product)
          .filter(removeEmptyProduct)

        var colmaxArr = that.productos[type]
          .filter(pr => pr.product === 'Colmax')
          .filter(colPr => colmaxMap[colPr.radar] === that.region)
          .filter(removeEmptyProduct)

        productsList = productsList.concat(colmaxArr)
      }

      return html`
        <div>
          <h3 class="subProductTypeTitle"> ${productsList.length !== 0 ? type : ''} </h3>
          <ul>
          ${productsList.map(buildListElem)}
          </ul>
        </div>
      `
      function buildListElem (elem) {
        return html`
        <li onclick=${productSelected} class="${isSelected()}">${elem.id}</li>
        `
        function isSelected () {
          return that.actual.id === elem.id
            ? 'selected'
            : ''
        }

        function productSelected () {
          emit('productTypeSelected', type)
          emit('newProduct', { id: elem.id, type: type })
        }
      }
    }
  }
  update (state, emit) {
    return state.productionActual != this.actual ||
      state.region !== this.region ||
      state.productos !== this.productos
  }
}
