const html = require('choo/html')
const css = require('sheetify')
const Nanocomponent = require('nanocomponent')

module.exports = class Logo extends Nanocomponent {
  constructor () {
    super()

    this.style = css`
    :host {
      grid-area:logo;
    }
    img {
      height:100%;
      margin: 2em 0 0 2em;
    }
    `
  }

  createElement () {
    return html`
      <div class=${this.style}>
        <img src="logo.png">
      </div>
    `
  }

  update () {
    return false
  }
}
