const html = require('choo/html')
const css = require('sheetify')
const Nanocomponent = require('nanocomponent')

module.exports.View = class Image extends Nanocomponent {
  constructor () {
    super()
    this.imgState = null
    this.zoomState = null

    this.style = css`
    :host {
      max-height: 70vh;
      overflow:hidden
    }
    :host img{
      object-fit: scale-down;
      width:auto;
      height:auto;
      display:block;
      margin:auto;
    }
    `
    this.loadingStyle = css`
    :host {
      position: relative;
      width: 10%;
      height: 10%;
      left: 50%;
      margin-top: 50%;
      background:var(--mainBg);
    }
    `
  }

  createElement ({ imgState, zoom }, emit) {
    this.imgState = imgState
    this.zoom = zoom
    var imgSrc = imgState.producto[imgState.atImg]
    var that = this

    var node = imgState.producto.length && imgSrc
      ? html`
      <div class=${this.style}>
        <img style="transform:scale(${zoom.state.scale});"
        style="transform-origin:${zoom.state.pos.x}px ${zoom.state.pos.y}px;"
          src=${imgSrc}></img>
      </div>`
      : imgState.loading
        ? html`${loadingAnim()}`
        : html`<div><img></img></div>`

    node.addEventListener('wheel', ev => {
      emit('zoom', ev)
    })

    node.addEventListener('click', ev => {
      ev.deltaY = 1.2
      emit('zoom', ev)
    })
    // Para escuchar el botón derecho uso el evento "contextmenu"
    node.addEventListener('contextmenu', (ev) => {
      ev.preventDefault()
      ev.deltaY = 0.5
      emit('zoom', ev)
    }, false)

    return node

    function loadingAnim () {
      return html`<div><img class=${that.loadingStyle} src="loader.gif"> </img></div>`
    }
  }

  update ({ imgState, zoom }) {
    return this.imgState.loading !== imgState.loading ||
           this.imgState.atImg !== imgState.atImg ||
           imgState.producto[imgState.atImg] ||
           this.zoom.state.scale !== zoom.state.scale
  }
}

module.exports.store = function (state, emitter) {
  var zoomInitState = {
    pos: {
      x: 0,
      y: 0
    },
    scale: 1
  }
  var zoom = {
    state: Object.assign({}, zoomInitState),
    params: {
      scaleAmt: 0.05
    }
  }
  state.zoom = zoom
  state.alt = false

  emitter.on('zoom', (e) => {
    // resetear zoom cuando estoy con el alt apretado
    if (state.alt) {
      state.zoom.state = Object.assign({}, zoomInitState)
      emitter.emit('render')
      return
    }

    var direction = 0
    e.deltaY > 1
      ? direction = -1
      : direction = 1

    // Si ya zoomoutié todo
    if (direction === -1 && zoom.state.scale === 1) {
      return
    }

    zoom.state.scale += zoom.params.scaleAmt * direction
    /*
    zoom.state.pos.x += -(e.clientX - e.offsetX) / zoom.state.scale
    zoom.state.pos.y += -(e.clientY - e.offsetY) / zoom.state.scale
    */
    zoom.state.pos.x = e.pageX - e.offsetX // zoom.state.scale
    zoom.state.pos.y = e.pageY - e.offsetY// zoom.state.scale

    emitter.emit('render')
  })

  emitter.on('keydown:alt', () => {
    state.alt = true
  })

  emitter.on('keyup:alt', () => {
    state.alt = false
  })
}
